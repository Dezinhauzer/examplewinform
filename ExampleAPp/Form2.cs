﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExampleAPp
{
    public partial class Form2 : Form
    {
        private readonly Form2State _form2State;
        public Form2(Form2State form2State)
        {
            InitializeComponent();
            _form2State = form2State;
            checkBox1.Checked = form2State.CheckBox1Checked;
            checkBox2.Checked = form2State.CheckBox2Checked;
            checkBox3.Checked = form2State.CheckBox3Checked;
            textBox1.Text = form2State.TextBox1;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) => _form2State.CheckBox1Checked = ((CheckBox)sender).Checked;

        private void checkBox2_CheckedChanged(object sender, EventArgs e) => _form2State.CheckBox2Checked = ((CheckBox)sender).Checked;

        private void checkBox3_CheckedChanged(object sender, EventArgs e) => _form2State.CheckBox3Checked = ((CheckBox)sender).Checked;

        private void textBox1_TextChanged(object sender, EventArgs e) => _form2State.TextBox1 = ((TextBox)sender).Text;
    }
}
