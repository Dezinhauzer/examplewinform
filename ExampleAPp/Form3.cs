﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ExampleAPp
{
    public partial class Form3 : Form
    {
        private readonly Form3State _form3State;

        public Form3(Form3State form3State)
        {
            InitializeComponent();
            _form3State = form3State;
            button1.Enabled = form3State.ButtonEnabled;
            checkBox1.Checked = form3State.CheckBoxChecked;
            label1.Text = form3State.LabelText;
            textBox1.Text = form3State.TextBoxText;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            _form3State.CheckBoxChecked = ((CheckBox)sender).Checked;
            _form3State.ButtonEnabled = button1.Enabled = _form3State.CheckBoxChecked;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _form3State.TextBoxText = ((TextBox)sender).Text;
            _form3State.LabelText = label1.Text = _form3State.TextBoxText;
        }
    }
}
