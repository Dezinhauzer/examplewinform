﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExampleAPp
{
    public partial class Form1 : Form
    {
        private readonly Form2State _form2State = new Form2State();
        private readonly Form3State _form3State = new Form3State();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            var f = new Form2(_form2State);
            f.Show();
            f.FormClosed += F_FormClosed;
        }

        private void F_FormClosed(object sender, FormClosedEventArgs e)
        {
            Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            var f = new Form3(_form3State);
            f.Show();
            f.FormClosed += F_FormClosed;
        }
    }
}
