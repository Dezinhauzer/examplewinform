﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleAPp
{
    public class Form3State
    {
        private bool _checkBoxChecked;
        private bool _buttonEnabled;
        private string _textBoxText;
        private string _labelText;



        public Form3State()
        {
        }

        public bool CheckBoxChecked { get => _checkBoxChecked; set => _checkBoxChecked = value; }
        public bool ButtonEnabled { get => _buttonEnabled; set => _buttonEnabled = value; }
        public string TextBoxText { get => _textBoxText; set => _textBoxText = value; }
        public string LabelText { get => _labelText; set => _labelText = value; }
    }
}
