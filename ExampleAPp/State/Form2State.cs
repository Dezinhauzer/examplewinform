﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleAPp
{
    public class Form2State
    {
        private bool _checkBox1Checked;
        private bool _checkBox2Checked;
        private bool _checkBox3Checked;
        private string _textBox1;

        public bool CheckBox1Checked { get => _checkBox1Checked; set => _checkBox1Checked = value; }
        public bool CheckBox2Checked { get => _checkBox2Checked; set => _checkBox2Checked = value; }
        public bool CheckBox3Checked { get => _checkBox3Checked; set => _checkBox3Checked = value; }
        public string TextBox1 { get => _textBox1; set => _textBox1 = value; }

        public Form2State()
        {
        }
    }
}
